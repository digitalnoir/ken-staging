<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * 
 * There is couple of blog style, please refer to the documentation
 * 1. Simple Blog - Left image with page excerpt on right side
 * 2. Blog with sidebar
 * 
 * The loop column can be set on individual template
 * 
 */

//include THEME_DIR . '/blocks/blogs/blog-simple.php';
include THEME_DIR . '/blocks/blogs/blog-with-sidebar.php';