<?php
    // This is the responsive menu
    // Always put this line in the footer.php before </body> tags
    // I put here so it's easily to manage
?>
<div class="hidden-lg hidden-print">
    <div id="menu-dropdown-trigger" class="burger-menu">

        <div class="search-resp">
            <a href="#" class="menu-search" title="Search"><i class="icon-search"></i></a>
        </div>

        <button class="hamburger hamburger--slider js-hamburger">
           <span class="hamburger-box"><span class="hamburger-inner"></span></span>
        </button>

    </div>

    <div id="menu-dropdown-resp">
        <?php include THEME_DIR . "/blocks/headers/menu-dropdown-resp.php"; ?>
    </div>
</div>

<?php 
    // Real header start here
?>
<header id="masthead" class="site-header menu-dropdown">
    
    <div class="site-branding">
        <a href="<?php echo get_home_url(); ?>" class="special-link"><img src="<?php echo THEME_URL; ?>/img/header-logo.jpg" alt=""></a>
    </div><!-- .site-branding -->

    <nav class="main-navigation">
        <?php
            // menu_class add has-search to append search button
        ?>
        <?php wp_nav_menu( array( 'theme_location' => 'header', 'menu_class'=> 'menu has-search', 'depth' => 2 ) ); ?>
       
    </nav><!-- .main-navigation -->

    <?php
        // Search Form - Padded
        // include THEME_DIR . "/blocks/headers/search-form-padded.php";

        // Search Form - Fullscreen
        include THEME_DIR . "/blocks/headers/search-form-fullscreen.php";
    ?>

</header><!-- #masthead -->
