<div class="dn-cta">
    <?php dn_enqueue_style('call-to-action') ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 cta-content">
                <div>
                    <h2><?php the_sub_field("title"); ?></h2>
                    <?php echo wpautop( get_sub_field("content") ); ?>
                </div>
            </div>
            <div class="col-sm-3 button-container">
                <?php render_link_helper('link', 'dn-button feature-button'); ?>
            </div>
        </div>
    </div>
</div>