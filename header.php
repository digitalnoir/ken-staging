<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php wp_title( '|', true, 'right' ); ?></title>
<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="page-wraper">

<?php 

	// Please change the scss file on \assets\production\sass\header\_header.scss
	// Use the correct scss

	include THEME_DIR . "/blocks/headers/menu-dropdown.php";
	//include THEME_DIR . "/blocks/headers/menu-side.php";
	//include THEME_DIR . "/blocks/headers/menu-blocky.php";

?>


<div id="content" class="page-content-wrapper">
