<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Digital_Noir_Starter_Pack
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="row">
			
				<div class="copy-text col-md-6 col-xs-12">
					<?php
                        $footer_text = get_field('footer_text', 'options');
                        $footer_text = str_replace('[year]', date('Y'), $footer_text);
                        echo $footer_text;
                    ?>
				</div>

				<div class="digital-noir col-md-6 col-xs-12">
					<a href="https://www.digital-noir.com" targe="_blank" rel="nofollow"><i class="icon-DN_black"></i></a>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>