jQuery(function ($) {

	$(document).ready(function (e) {

		HeaderMenuFunction();

		GravityFormFunctionality();




		//StickHeaderFunctionality();

		//BurgerMenuFunctionality();

		//AccordianMenuFunctionality(); 

		//DropDownMenuFunctionalty();

		//SlideFullMenuFunctionality();

		//SlickSliderFunctionalty();
 
		//DownArrowFunctionalty();
 
		//GoogleMapsFunctionality();

		//MasonaryLayoutFunctionality();

		//DynamicLoadPostsFunctionality();

		//SearchDropFunctionality();

		//CartDropFunctionality();

		//WooShopIsotopeFunctionality();

		//WooTabFunctionality();

		//InstagramFunctionality();

		//CookiesFooterFunctionality();

	})


	// All Header Options
	function HeaderMenuFunction(){

		// tell when to run the stuck
		var $offset = $('#masthead').attr('data-offset') == 'true' ? $('#masthead').height() + 10 : 30

		// Sticky header
		$(window).scroll(function () {
			if($(document).scrollTop() > $offset){
				$('body').addClass('header-stuck')
			}else{
				$('body').removeClass('header-stuck')
			}
		});

		// Burger menu toggle
		$(document).on('click', 'button.js-hamburger', function(e){
			e.preventDefault()
			$(this).toggleClass('is-active')
			$('body').toggleClass('mobile-menu-open')
			
			// hook so other function can runnig
			$(window).trigger('hamburger-menu-click')
		})

		// Disable parent menu to be clickable
		$('.main-navigation li.menu-item-has-children > a').on('click',function(e){
			e.preventDefault()
		})


		// Open close search
		$('.menu-search').click(function(e){
			e.preventDefault()
			$('body').toggleClass('search-open')
			$('.header-search-form .search-field').focus()

			// fullscreen style
			if($('.header-search-form').hasClass('fullscreen')){
				$('.header-search-form').fadeToggle(200,function(){
					// focus the search
					$('.header-search-form .search-field').focus()
				})
			}
		})

		// close fullscreen
		$('.fullscreen-shader').click(function(e){
			e.preventDefault()
			$('body').removeClass('search-open')
			$('.header-search-form').fadeOut(200)
		})

	}


	// Gravity Label Functionality
	function GravityFormFunctionalitySetup() {

		// label hover style
		$('li.gfield input, li.gfield textarea').on('change keyup focusout', function(){
			
			var isEmpty = $(this).val()
			if(isEmpty == ''){
				$(this).parents('li.gfield').removeClass('filled')
			}else{
				$(this).parents('li.gfield').addClass('filled')
			}

		})

		// trigger by default
		$('li.gfield input, li.gfield textarea').trigger('focusout')

		// label hover style
		$('li.gfield input, li.gfield textarea').on('focusin', function(){
			
			$(this).parents('li.gfield').addClass('filled')
			
		})

		// Scroll the user to the top of the form if there is an error on this page
		if ($(".gform_wrapper.gform_validation_error").length > 0) {

			$('html, body').animate({
				scrollTop: $(".gform_wrapper").offset().top - 140
			}, 1);
		}

		// Setup this label for movement
		// Form choice placeholder
		$(".form-label-move").find(".ginput_container.ginput_container_text, .ginput_container.ginput_container_textarea, .ginput_container.ginput_container_email, .dn-form-input-container").each(function () {

			// Set this label up for greateness
			$(this).siblings("label").addClass("text-field");

			// On focusing of this elements
			$(this).find("input, textarea").focusin(function () {
				$(this).parent().siblings("label").addClass("focus");
			});

			// If any of these fields already have content
			$(this).find("input, textarea").each(function () {

				if ($(this).val() != '') {
					$(this).parent().siblings("label").addClass("focus");
				}
			});

			// On stop focusing of this elements
			$(this).find("input, textarea").focusout(function () {

				// If this field has no contents
				if ($(this).val() == '') {
					$(this).parent().siblings("label").removeClass("focus");
				}
			});

		});

		// Setup live form validation
		// Add required fields in place of grav forms classes
		$(".gfield_contains_required").find('input[type="text"], input[type="email"], input[type="url"], input[type="password"], input[type="search"], input[type="number"], input[type="tel"],  input[type="date"], input[type="month"], input[type="week"], input[type="time"], input[type="datetime"], input[type="datetime-local"], input[type="color"], textarea').addClass("required");

		// On unclicking a form field
		$("input.required:not(.ignore), textarea.required:not(.ignore), .gfield_contains_required select").focusout(function () {

			if ($(this).val() == '') {
				$(this).addClass("error");
			} else {
				$(this).removeClass("error");
				$(this).parents(".gfield.gfield_error").removeClass("gfield_error");
			}
		});
	}



















	



	// service accordion
	$(document).on('click','.dn-services .service-item .heading:not(.disable-accordion)',function(e){
	
		e.preventDefault()
		$(this).parent().toggleClass('active')
		$(this).siblings('.content').slideToggle()

	})

	// slide menu
	function SlideFullMenuFunctionality(){

		if( $('.header-slide-full').length > 0 ){

			var count = 0;
			$('.header-slide-full .the-menu .menu > li').each(function(i,e){
				$(this).addClass('delay-'+i)
				count++
			})

			// the link
			$('.header-slide-full .links').addClass('delay-'+ (count+1) )

		}

	}





	// Creates functionality for burger menus
	function BurgerMenuFunctionality() {

		if ($('#burger,#burger-blocky').length > 0) {

			var MenuOpenReady = true;

			$('#burger,#burger-blocky').click(function () {

				if (MenuOpenReady) {

					MenuOpenReady = false;
					setTimeout(function () {
						MenuOpenReady = true;
					}, 250);

					if ($('#burger,#burger-blocky').hasClass("open")) {
						CloseMenu();

					} else {
						OpenMenu();
					}
				}
			});

		}

	}

	function OpenMenu() {

		// Close the search
		CloseSearchDrop();

		// Close the side-cart
		CloseSideCart();

		$("#header-menu").addClass("appear");
		$("#masthead").addClass("filled-menu");
		$("body").addClass("mobile-menu-open");
		$(".hamburger-blush").addClass('is-active');
		$("#burger,#burger-blocky").addClass('open');
		ShowMenuShader();
		LockPageScroll();
	}

	function CloseMenu() {
		$("#header-menu").removeClass("appear");
		$("body").removeClass("mobile-menu-open");
		$("#masthead").removeClass("filled-menu");
		$(".hamburger-blush").removeClass('is-active');
		$("#burger,#burger-blocky").removeClass('open');
		HideMenuShader();
		UnlockPageScroll();
	}

	// Creates functionality for accordian menus
	function AccordianMenuFunctionality() {

		// For each accordian
		$(".accordian").each(function () {

			// Hide the icons for those menu elements that do not have children
			$(this).find("li").each(function () {

				if (!$(this).find("ul.children, ul.sub-menu").length > 0) {
					$(this).find("i").hide();
				}

			});

			// Setup the menu
			$(this).find(".sub-menu, .children").each(function () {
				$(this).hide();
				$(this).parent().addClass("accord-parent");
			});

			// CLick functionality
			$(this).find("li i:last-of-type()").hide();
			$(this).find("li i").mousedown(function () {

				$(this).toggle();
				$(this).siblings("i").toggle();

				$(this).parent().find(".sub-menu, .children").slideToggle(200);

			});

		});

		$(".accordian-menu-disable-parent").each(function () {

			// Hide the icons for those menu elements that do not have children
			$(this).find("li").each(function () {

				if (!$(this).find("ul.children, ul.sub-menu").length > 0) {
					$(this).find("i").hide();
				}

			});

			// Setup the menu
			$(this).find(".sub-menu, .children").each(function () {
				$(this).hide();
				$(this).parent().addClass("accord-parent");
			});

			// CLick functionality
			$(this).find("li i:last-of-type()").hide();
			$(this).find("li i").mousedown(function () {

				$(this).toggle();
				$(this).siblings("i").toggle();

				$(this).parent().find(".sub-menu, .children").slideToggle(200);

			});

			$(this).find("ul.menu-item-has-children > li > a").on('click', function (e) {

				e.preventDefault();

				$(this).siblings("i").toggle();

				$(this).parent().find(".sub-menu, .children").slideToggle(200);

			});

		});


		// For each accordian
		$(".accordian-category .toggle-open").click(function (e) {
			e.preventDefault()

			var $this = $(this)
			if (($this).hasClass('icon-plus')) {
				$this.removeClass('icon-plus')
				$this.addClass('icon-minus')
				$this.siblings('.children').slideDown()
			} else {
				$this.removeClass('icon-minus')
				$this.addClass('icon-plus')
				$this.siblings('.children').slideUp()
			}

			// reset the others
			$this.parent().siblings('li').find('.children').slideUp()
			$this.parent().siblings('li').find('.toggle-open').removeClass('icon-minus')
			$this.parent().siblings('li').find('.toggle-open').addClass('icon-plus')


		})

		// Open by default
		if ($(document).find('.accordian-category .current-cat').length > 0) {
			var $current_cat = $(document).find('.accordian-category .current-cat');
			$current_cat.parent().siblings('.toggle-open').trigger('click')
			if ($current_cat.parent().hasClass('accordian-category')) {
				$current_cat.find('.toggle-open').trigger('click')
			}
		}
	}

	// Creates functionality for dropdown menus
	function DropDownMenuFunctionalty() {

		if ($(".menu-container.drop-menu").length > 0) {

			// Hide those & sub-menus icons properly
			$(".menu-container.drop-menu .sub-menu").hide();
			$(".menu-container.drop-menu").removeClass("hidden-sub");

			// Setup hover effects
			$(".menu-container.drop-menu li.menu-item-has-children").mouseenter(function () {

				ClearCurrentSubMenu();

				// Setup this class
				$(this).addClass("dropped");
				$(this).find(".sub-menu").show();

			});

			$(".menu-container.drop-menu li.menu-item-has-children").mouseleave(function () {
				ClearCurrentSubMenu();
			});

			// Make top level menu items not clickable in the header (on desktop)
			$(".menu-container.drop-menu li.menu-item-has-children > a").on('click', function (e) {
				e.preventDefault();
			});

			// Hide all other sub-menu's
			function ClearCurrentSubMenu() {
				$(".menu-container.drop-menu li.menu-item-has-children.dropped .sub-menu").hide();
				$(".menu-container.drop-menu li.menu-item-has-children.dropped").removeClass("dropped");
			}
		}
	}

	// Sets up various slick sliders
	function SlickSliderFunctionalty() {

		// Regular slider container
		if ($(".slick-slider-container").length > 0) {
			if ($(".slick-slider-container").children().length > 1) {
				$(".slick-slider-container").each(function () {
					var $this = $(this)
					if ($this.hasClass('testimonial-slider')) {

						$this.slick({
							arrows: false,
							dots: true,
							infinite: true,
							autoplay: false,
							adaptiveHeight: true
						});

						/* $this.on('setPosition', function () {
							$this.find('.slick-slide').matchHeight();
						}) */

					} else {
						$this.slick({
							arrows: false,
							dots: true,
							infinite: true,
							autoplay: true,
							adaptiveHeight: true
						});
					}

				})

			}
		}

		// text gallery
		if($('.text-gallery-slider').length > 0){
			$('.text-gallery-slider').each(function(){
				
				var $responsive = [];
				var $this = $(this)

				// responsive option
				if($this.hasClass('responsive-centered')){
					$responsive = [
						{
							breakpoint: 991,
							settings: {
								dots: false,
								arrows: false,
								infinite: false,
								autoplay: true,
								centerMode: true,
								slidesToShow: 1,
								slidesToScroll: 1,
							}
						}
					]
				}

				$this.slick({
					arrows: false,
					dots: true,
					infinite: true,
					autoplay: true,
					adaptiveHeight: false,
					responsive: $responsive
				});

				/* $this.slick({
					dots: false,
					arrows: false,
					infinite: false,
					autoplay: true,
					centerMode: true,
					responsive: [
						{
							breakpoint: 9999,
							settings: "unslick"
						},
						{
							breakpoint: 991,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
							}
						}
					]
				}); */
			})
		}

		// Mega slider container
		if ($(".mega-slick-slider-container").length > 0) {
			if ($(".mega-slick-slider-container").children().length > 1) {
				$('.mega-slick-slider-container').slick({
					dots: true,
					infinite: true,
					autoplay: true,
					prevArrow: "<i class='icon-chevron-left slick-prev'></i>",
					nextArrow: "<i class='icon-chevron-right slick-next'></i>",
				});
			}
		}

		

		// Feature 
		if ($(".dn-feature-carousel").length > 0) {

			// Main slick slider
			$(".dn-feature-carousel .main-image").slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				asNavFor: '.dn-feature-carousel .selector'
			});

			// Carousel slider
			$(".dn-feature-carousel .selector").slick({
				slidesToShow: 3,
				slidesToScroll: 1,
				prevArrow: "<i class='icon-chevron-left slick-prev'></i>",
				nextArrow: "<i class='icon-chevron-right slick-next'></i>",
				asNavFor: '.dn-feature-carousel .main-image',
				dots: false,
				centerMode: true,
				focusOnSelect: true,
				responsive: [{
					breakpoint: 991,
					settings: {
						slidesToShow: 2,
					}
				}, ]
			});

			// Main lightboxes
			$('.dn-feature-carousel .main-image .image-container').magnificPopup({
				type: 'image',
				gallery: {
					enabled: true
				},
			});

			// Carousel lightboxes
			$('.dn-feature-carousel .selector .image-container').magnificPopup({
				type: 'image',
				gallery: {
					enabled: true
				},
			});

		}

		// Product 
		if ($(".dn-product-carousel").length > 0) {

			$(".dn-product-carousel .main-image").slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: true,
				fade: true,
				infinite: false,
				asNavFor: '.dn-product-carousel .selector',
				prevArrow: "<i class='icon-chevron-left slick-prev'></i>",
				nextArrow: "<i class='icon-chevron-right slick-next'></i>",
			});
			$(".dn-product-carousel .selector").slick({
				slidesToShow: 4,
				slidesToScroll: 4,
				infinite: false,
				asNavFor: '.dn-product-carousel .main-image',
				dots: false,
				vertical: true,
				arrows: false,
				centerMode: false,
				focusOnSelect: true,
				responsive: [{
					breakpoint: 991,
					settings: {
						slidesToShow: 2,
					}
				}, ]
			});

			// Create lightboxes
			$('.dn-product-carousel .main-image .image-container').magnificPopup({
				type: 'image',
			});

		}

		// Product-mini 
		if ($(".dn-product-carousel-mini").length > 0) {

			$(".dn-product-carousel-mini .slider").slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				dots: true,
				fade: true,
				asNavFor: '.dn-product-carousel .selector',
			});

			// Create lightboxes
			$('.dn-product-carousel .main-image .image-container').magnificPopup({
				type: 'image',
				gallery: {
					enabled: true
				},
			});

		}
	}

	// Creates funcitonality for the down arrow
	function DownArrowFunctionalty() {

		// If the user clicks on the next section arrow
		$("#dn-next-sec-arrow").mousedown(function () {

			// Find the scrolltop height for this objects parent
			var offSetTop = $(this).parent().offset().top +
				$(window).height() // Add the window height
				-
				$("#masthead").outerHeight(); // minus the menu top height

			// Scroll to the next section
			$('html, body').animate({
				scrollTop: offSetTop
			}, 500);

		});
	}




	function OpenNewsFilter() {

		// Close the search & mini-cart
		CloseSearchDrop();
		CloseSideCart();

		// fade in the BG
		$("#post-filter-bg").fadeIn(250);

		// Bring in the news filter from the side
		$("#post-filter-container").addClass("appear");
		$("#filter-bottom").addClass("appear");

		// Lock the background
		LockPageScroll();
	}

	function CloseNewsFilter() {

		// Create the final list of categories to display
		var finalCatList = "";
		var finalCatListNice = "";

		// Scan the list for the categories
		$("#post-filter-container .middle a.selected").each(function () {

			if (finalCatList != "") {
				finalCatList += ", ";
			}
			finalCatList += "." + RegexReplace($(this).text());

			if (finalCatListNice != "") {
				finalCatListNice += ", ";
			}
			finalCatListNice += $(this).text();
		});

		// Show only those in the Masonary display
		// Display all cats
		if (finalCatList == "") {

			$(".masonary-layout-container").isotope({
				filter: "*"
			})

			// Display just the requires cats
		} else {

			$(".masonary-layout-container").isotope({
				filter: finalCatList
			})

		}

		// Show/Hide no posts
		if (!$(".masonary-layout-container").data('isotope').filteredItems.length) {
			$("#no-posts").show();
		} else {
			$("#no-posts").hide();
		}

		// console.log( finalCatList );

		// Update the Filter news with the current cats showing
		if (finalCatListNice != "") {
			$("#post-filter .results").text(': ' + finalCatListNice);
		} else {
			$("#post-filter .results").text("");
		}

		// fade in the BG
		$("#post-filter-bg").fadeOut(250);

		// Bring in the news filter from the side
		$("#post-filter-container").removeClass("appear");
		$("#filter-bottom").removeClass("appear");

		// Unlock the background
		UnlockPageScroll();

		// Scroll user back to the top of the section
		$('html, body').animate({
			scrollTop: $("#main").offset().top - 100
		}, 500);
	}

	function DynamicLoadPostsFunctionality() {

		// Keep track of how many posts have loaded
		$("#dn-dynamic-load-posts .dn-button").hide();
		$("#dn-dynamic-load-posts .dn-button").removeClass("hidden");

		CheckForLoadPosts();
		$(window).scroll(function () {
			CheckForLoadPosts();
		});

		$("#dn-dynamic-load-posts .dn-button").click(function (e) {

			// Hide the button
			e.preventDefault();
			$(this).hide();

			PostLoaderFunctionality();

		});

		function CheckForLoadPosts() {

			if ($("#dn-dynamic-load-posts").length > 0 && !LoadingMorePosts) {

				// Calculate the pixel distance of the bottom of the screen
				var windowBottomDistanceCalc = $(window).scrollTop() + $(window).height();

				// If this is the first 2 times we're autoloading posts
				if (PostsLoaded < PostLoadsBeforeButton) {

					// If we've reached the final blog post and we're not currently loading other blog posts
					if (windowBottomDistanceCalc > $("#dn-dynamic-load-posts").offset().top) {

						PostLoaderFunctionality();
					}

				}

			}
		}

		function PostLoaderFunctionality() {

			PostsLoaded++;

			// indicate we're loading more blog posts
			LoadingMorePosts = true;

			// Show the post loading element
			$("#dn-dynamic-load-posts .post-loading").removeClass("hidden");

			// Indicate the miniumum loading time has not been met yet
			MinLoadingWaitingTimeCompleted = false;

			// Load the blog posts
			LoadMorePosts(5);

			// Gives a minimum loading time to show users the loading thing
			setTimeout(function () {
				MinLoadingWaitingTimeCompleted = true;
			}, MinLoadingTime);
		}
	}

	function LoadMorePosts(NumberOfPostsToLoad) {

		// find the number of blog articles here
		var PostCount = $(".dn-single-post").length;

		// If this is a categories page
		var cattype = "";
		if ($("#cat-type").length > 0) {
			cattype = $("#cat-type").attr("data-cattype");
		}

		jQuery.ajax({
			url: dn_variable.ajax_url,
			type: 'post',
			data: {
				action: 'post_entries_find_entries',
				current_posts: PostCount,
				post_type: 'post',
				cattype: cattype,
				fetch_amount: NumberOfPostsToLoad,
			},
			success: function (data) {
				LoadMorePosts_Success(data);
			},
			error: function (data) {
				console.log("AJAX request failed");
			}
		});
	}

	function LoadMorePosts_Success(data) {

		CompleteLoadingFunctionality();

		function CompleteLoadingFunctionality() {

			// if the minmum laoding time has been met
			if (MinLoadingWaitingTimeCompleted) {

				// Hide the post loading element
				$("#dn-dynamic-load-posts .post-loading").addClass("hidden");

				// Post the new blog posts data into the page
				var json = JSON.parse(data);

				// Paste the actual data ino 
				$("#dn-dynamic-load-posts").before(json.html);

				// If there are not more blog posts to load, delete the loader element
				if (json.is_final) {
					$("#dn-dynamic-load-posts").remove();
				}

				// Indicate we're finished loading blog posts
				LoadingMorePosts = false;

				// Show the button if the posts loading is appropriate
				if (PostsLoaded >= PostLoadsBeforeButton) {
					$("#dn-dynamic-load-posts .dn-button").show();
				}

			} else {

				// Check again shortly
				setTimeout(function () {
					CompleteLoadingFunctionality();
				}, 100);
			}
		}
	}

	function SearchDropFunctionality() {

		if ($("#dn-search-drop").length > 0 && $("#dn-search-drop-button").length > 0) {

			// close button
			$('#dn-search-drop.full-style .title-close a, #dn-search-drop.full-style .shade').click(function(e){
				e.preventDefault()
				$('#dn-search-drop').toggleClass('open-full-search')
			})

			$("#dn-search-drop-button").click(function () {

				if( $(this).hasClass('full-page') ){
					
					// Open full drop search
					$('#dn-search-drop').toggleClass('open-full-search')

				}else{
					// Open the search
					if (!$("#dn-search-drop").hasClass("dropped")) {

						OpenSearchDrop();

						// Close the search
					} else {

						CloseSearchDrop();

					}
				}

			});

		}
	}

	function OpenSearchDrop() {

		// Close the menu
		CloseMenu();

		// Close the side-cart
		CloseSideCart();

		$("#dn-search-drop").addClass("dropped");
	}

	function CloseSearchDrop() {
		$("#dn-search-drop").removeClass("dropped");
	}

	function CartDropFunctionality() {

		if ($("#dn-side-cart-menu").length > 0 && $("#dn-cart-pop-button").length > 0) {

			$(document).on("click touchstart", "#dn-cart-pop-button", function () {
				OpenSideCart();
			});

			$("#close-dn-side-cart-menu").click(function () {

				CloseSideCart();
			});

		}

	}

	function OpenSideCart() {

		// Close the search
		CloseSearchDrop();

		// Close the menu
		CloseMenu();

		// Open the cart
		$("#dn-side-cart-menu").addClass("appear");

		// Lock the page scrolling
		LockPageScroll();
	}

	function CloseSideCart() {

		// Close the cart
		$("#dn-side-cart-menu").removeClass("appear");

		// Allow page to scroll again
		UnlockPageScroll();
	}

	function GravityFormFunctionality() {

		// Run once on startup
		GravityFormFunctionalitySetup();

		// Run on failed sbumissions
		jQuery(document).bind('gform_post_render', function () {
			if ($(".validation_error").length > 0) {
				GravityFormFunctionalitySetup();
			}
		});
	}

	

	var scrollPosition;
	var scrollLocked = false;

	function LockPageScroll() {
		/* scrollPosition = [
			self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
			self.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop
		];
		var html = jQuery('body'); // it would make more sense to apply this to body, but IE7 won't have that
		scrollLocked = true;
		html.css({
			'position': 'fixed',
			"left": "0",
			"top": "-" + scrollPosition[1] + "px",
			"overflow": "hidden",
			"width": "100%",
		}); */
	}

	function UnlockPageScroll() {
/* 
		// If the page scroll is locked
		if (scrollLocked) {
			var html = jQuery('body');
			scrollLocked = false;
			html.css({
				'position': 'static',
				"left": "auto",
				"top": "auto",
				"overflow": "visible",
				"width": "auto",
			});
			window.scrollTo(scrollPosition[0], scrollPosition[1]);
		} */
	}

	function WooShopIsotopeFunctionality() {

		// if this is the woo shop page
		if ($("body.post-type-archive-product.woocommerce").length > 0) {

			// ASSIGN ISOTOPE
			var $container = $('.product-loop-container');
			var buttonFilter = '*';
			$container.isotope({
				itemSelector: '.product',
				resizable: false, // disable normal resizing
				layoutMode: 'fitRows',
				filter: function () {
					var $this = $(this);
					var rangeFilter = $('#woo-filter-price');
					var RangeMin = parseFloat(rangeFilter.attr('data-filter-min'))
					var RangeMax = parseFloat(rangeFilter.attr('data-filter-max'))
					var price = parseFloat($this.attr('data-price'))
					var isPriceRange = price <= RangeMax && price >= RangeMin

					return $this.is(buttonFilter) && (isPriceRange);
				}
			});

			// FILTER CATEGORY
			$('.dn-woo-category-filter li a').click(function (e) {
				e.preventDefault()
				$(this).toggleClass('active')
				var btnFilter = []
				$('.dn-woo-category-filter li a.active').each(function () {
					btnFilter.push($(this).attr('data-filter'))
				})
				buttonFilter = btnFilter.join(',');
				if (buttonFilter == '') {
					buttonFilter = '*';
				}
				$container.isotope()
			})

			// FILTER PRICE
			var $priceFilterContainer = $('#woo-filter-price');
			var $priceMin = parseFloat($priceFilterContainer.attr('data-min'));
			var $priceMax = parseFloat($priceFilterContainer.attr('data-max'))
			$priceFilterContainer.slider({
				range: true,
				min: $priceMin,
				max: $priceMax,
				values: [$priceMin, $priceMax],
				create: function (e, ui) {
					$priceFilterContainer.find('span.ui-slider-handle:eq(0)').addClass('range-min').html('<span class="slide-price">$' + $priceFilterContainer.slider("values", 0) + '</span>')
					$priceFilterContainer.find('span.ui-slider-handle:eq(1)').addClass('range-max').html('<span class="slide-price">$' + $priceFilterContainer.slider("values", 1) + '</span>')
				},

				slide: function (event, ui) {
					$(ui.handle).html('<span class="slide-price">$' + ui.value + '</span>');
					$priceFilterContainer.attr('data-filter-min', $priceFilterContainer.slider("values", 0))
					$priceFilterContainer.attr('data-filter-max', $priceFilterContainer.slider("values", 1))
				},

				stop: function (event, ui) {
					$container.isotope()
				}

			})

		}
	}

	function WooTabFunctionality() {

		///////////////////////////
		// Desktop functionality //
		///////////////////////////

		// Set the first tab to active
		$(".dn-wc-tabs > div:first-child").addClass("active-desktop");
		$(".dn-tabs-panel:first").addClass("active-desktop");

		// If a user clicks on a tab
		$('.tabs-accordion-header a').click(function (e) {

			// Prevent default behaviour
			e.preventDefault()

			// Remove the currently active class
			$('.tabs-accordion-header').removeClass("active-desktop");
			$(".dn-tabs-panel").removeClass("active-desktop");

			// Make this button & tab active
			var ItemPosition = $(this).parent().index();
			$(this).parent().addClass("active-desktop");
			$(".dn-tabs-panel").eq(ItemPosition).addClass("active-desktop");

		})

		////////////////////////
		// Resp functionality //
		////////////////////////

		// Set the first tab to active
		$('.dn-tabs-panel:first .tabs-accordion-header a').addClass("active-resp");
		$(".dn-tabs-panel:first .dn-tabs-inner").addClass("active-resp");

		// If a user clicks on a tab
		$('.tabs-accordion-header-resp a').click(function (e) {

			// if this class is not currently active
			if (!$(this).hasClass(".active-resp")) {

				// Prevent default behaviour
				e.preventDefault()

				// Remove the currently active class
				$('.tabs-accordion-header-resp a.active-resp').removeClass("active-resp");
				$(".dn-tabs-inner.active-resp").removeClass("active-resp");

				// Make this the new active class
				$(this).addClass("active-resp");
				$(this).parent().siblings(".dn-tabs-inner").addClass("active-resp");

			}

		})

	}

	function RegexReplace(ArgString) {

		return ArgString.replace(/[\W\s\/]+/g, '_');

	}

	

	function InstagramFunctionality() {

		// If te instagram container is shown on the page
		if ($("#dn-instafeed").length > 0) {

			// Max of 12 available
			var ImagesToPull = 8;

			jQuery.ajax({
				url: dn_variable.ajax_url,
				type: 'post',
				data: {
					action: 'dn_insta_js_pull_func',
					ImagesToPull: ImagesToPull,
				},
				success: function (data) {

					// Convert the json data
					data = JSON.parse(data);

					// Loop through each of the images
					data['images'].forEach(function (element) {

						// Create strig to be added
						var temp_add_string = '<a href="https://www.instagram.com/p/' + element['shortcode'] + '" target="_blank" rel="nofollow" class="insta-image special-link" style="background-image: url(' + element['image'] + ');"></a>';

						$("#dn-instafeed").append(temp_add_string);

					});

				},
				error: function (data) {
					console.log("AJAX request failed");
				}
			});

		}

	}

	function CookiesFooterFunctionality() {

		$("#cookies-footer-notice a.dn-button").click(function(e){

			// Stop the button from doing it's thinggg
			e.preventDefault();

			// Add a cookie to the page
			setCookie( "site-cookies-agreement", "site-cookies-agreement-agreed", 99 );
			
			// Make it dissapear
			$("#cookies-footer-notice").slideUp();
			
		});

	}

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

});